﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private PlayerInfo PlayerInfo;
    [SerializeField] private Card KingCard;
    [SerializeField] private Deck Deck;

    private List<Card> handCards; 

    public int wonRounds = 0;

    private int m_closeRangeAttack = 0;
    private int m_midRangeAttack = 0;
    private int m_largeRangeAttack = 0;


    void Start()
    {
        handCards = new List<Card>();
        //handCards.Add(Deck.GetCard());
    }

    public int GetAttackForce()
    {
        return m_closeRangeAttack + m_midRangeAttack + m_largeRangeAttack; 
    }

    public int GetCurrentCardsInHandCount()
    {
        return handCards.Count; 
    }
}
