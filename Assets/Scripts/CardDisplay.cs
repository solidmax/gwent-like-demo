﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro; 

public class CardDisplay : MonoBehaviour
{
    public Card Card;

    public Image Artwork; 
    public TextMeshProUGUI CardName;
    public TextMeshProUGUI Description;
    public TextMeshProUGUI Attack;
    public TextMeshProUGUI Faction;
    public TextMeshProUGUI Range;

    void Start()
    {
        if(!Card)
        {
            Debug.Log("card not loaded!");
            return; 
        }

        Artwork.sprite = Card.Artwork; 
        CardName.text = Card.CardName;
        Description.text = Card.Description;
        Attack.text = Card.AttackForce.ToString();
        Faction.text = Card.Faction.ToString();
        Range.text = Card.Range.ToString();

    }
}
