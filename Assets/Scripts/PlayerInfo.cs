﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Player", menuName = "PlayerInfo")]
public class PlayerInfo : ScriptableObject
{
    public string PlayerName;
    public Sprite Artwork; 
    public GlobalVariables.Factions Faction;

    public Deck Deck; 
}
