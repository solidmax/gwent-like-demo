﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Deck", menuName = "Deck")]
public class Deck : ScriptableObject
{
    public List<Card> m_cards;
    public int MaxCount = 30; 

    public Card GetCard(string requestedCardName)
    {
        if(m_cards.Count == 0)
        {
            Debug.Log("Empty deck");
            return null;
        }

        foreach (Card card in m_cards)
        {
            if (card.CardName == requestedCardName)
            {
                return card; 
            }
        }
        Debug.Log("Card not founded");
        return null; 
    }
}
