﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalVariables
{
    public enum CardRange
    {
        Close,
        Mid,
        Long
    }

    public enum Factions
    {
        Monsters,
        Nilfgaardian,
        ScoiaTael,
        Skellige,
        NorthernRealms,
        Neutral
    }
}
